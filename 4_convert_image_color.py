"""
Load an image from a given path, change the color to gray and show it in a window
"""
# library imports
import cv2

# load image from cv2.imread(<path>)
img = cv2.imread("data/1/lenna.png")

# convert image from RGB to another color space
gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)
# gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

# show image by creating a new window called "image" and pass it the img variable
cv2.imshow("image", gray_img)

# wait for any key press to close window
cv2.waitKey(0)
