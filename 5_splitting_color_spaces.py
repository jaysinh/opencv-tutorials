"""
Load an RGB image and split it into 3 individual channels
"""
import cv2

# load an image
img = cv2.imread("data/1/lenna.png")

# get the width, height and channels of loaded image
height, width, channels = img.shape

# create blue channel image
blue_image = img.copy()
blue_image[:, :, 1] = 0
blue_image[:, :, 2] = 0

# create green channel image
green_image = img.copy()
green_image[:, :, 0] = 0
green_image[:, :, 2] = 0

# create red channel image
red_image = img.copy()
red_image[:, :, 0] = 0
red_image[:, :, 1] = 0

# show images
cv2.imshow("blue channel", blue_image)
cv2.imshow("green channel", green_image)
cv2.imshow("red channel", red_image)

# wait for key press
cv2.waitKey(0)

# destry all open windows
cv2.destroyAllWindows()
