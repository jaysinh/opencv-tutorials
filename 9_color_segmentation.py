"""
Load an image and perform color segmentation using HSV color space
"""
import cv2
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib import colors
import matplotlib.pyplot as plt
import numpy as np

# This method created a scatter plot of the image in RGB color space
def createRGBScatterPlot(image):
    # convert from BGR to RGB as opencv loads images with the red and blue channels flipped
    # this is necessary only for showing using matplotlib as it expects RGB color order
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # split the channels
    r, g, b = cv2.split(image)

    # initialize a 3D pyplot figure
    fig = plt.figure()
    fig.canvas.set_window_title('RGB Color Plot')
    axis = fig.add_subplot(1, 1, 1, projection="3d")

    # set the pixel colors to the corresponding plot points colors
    pixel_colors = image.reshape((np.shape(image)[0]*np.shape(image)[1], 3))
    norm = colors.Normalize(vmin=-1., vmax=1.)
    norm.autoscale(pixel_colors)
    pixel_colors = norm(pixel_colors).tolist()
    
    # create a scatter plot with labels
    axis.scatter(r.flatten(), g.flatten(), b.flatten(), facecolors=pixel_colors, marker=".")
    axis.set_xlabel("Red")
    axis.set_ylabel("Green")
    axis.set_zlabel("Blue")
    plt.show()

# This method created a scatter plot of the image in HSV color space
def createHSVScatterPlot(image):
    # convert from BGR to HSV color space
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image_hsv = cv2.cvtColor(image_rgb, cv2.COLOR_RGB2HSV)

    # split the channels
    h, s, v = cv2.split(image_hsv)

    # initialize a 3D pyplot figure
    fig = plt.figure()
    fig.canvas.set_window_title('HSV Color Plot')
    axis = fig.add_subplot(1, 1, 1, projection="3d")

    # set the pixel colors to the corresponding plot points colors. Here, we use the RGB color space 
    # as matplotlib can show colors only in RGB
    pixel_colors = image_rgb.reshape((np.shape(image_rgb)[0]*np.shape(image_rgb)[1], 3))
    norm = colors.Normalize(vmin=-1., vmax=1.)
    norm.autoscale(pixel_colors)
    pixel_colors = norm(pixel_colors).tolist()

    # create a scatter plot with labels
    axis.scatter(h.flatten(), s.flatten(), v.flatten(), facecolors=pixel_colors, marker=".")
    axis.set_xlabel("Hue")
    axis.set_ylabel("Saturation")
    axis.set_zlabel("Value")
    plt.show()

# HSV value change listener
def onChange(x):
    # set the lower threshold HSV values
    low = (hue_val_low, saturation_val_low, value_val_low)
    # set the higher threshold HSV values
    high = (hue_val_high, saturation_val_high, value_val_high)

    # create a binary mask by keeping values that lie between high and low values
    mask = cv2.inRange(img_hsv, low, high)

    # result is a bitwise and operator that sets the x,y HSV pixel value only if mask is 1.
    # Sets HSV pixel value to 0 if mask value is 0
    result = cv2.bitwise_and(img_hsv, img_hsv, mask=mask)

    # show result in HSV
    cv2.imshow("Result HSV", result)

    # show result in RGB
    result = cv2.cvtColor(result, cv2.COLOR_HSV2BGR)
    cv2.imshow("Result RGB", result)



# load image from cv2.imread(<path>)
img = cv2.imread("data/9/red_ball.jpg")

# createRGBScatterPlot(img)
# createHSVScatterPlot(img)

# create copies of image in RGB and HSV
img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
img_hsv = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2HSV)

# initialize display windows
cv2.namedWindow("Original")
cv2.namedWindow("Result RGB")
cv2.namedWindow("Result HSV")

# initialize trackbars
cv2.createTrackbar("Hue Low", "Original", 0, 255, onChange)
cv2.createTrackbar("Hue High", "Original", 255, 255, onChange)
cv2.createTrackbar("Saturation Low", "Original", 0, 255, onChange)
cv2.createTrackbar("Saturation High", "Original", 255, 255, onChange)
cv2.createTrackbar("Value Low", "Original", 0, 255, onChange)
cv2.createTrackbar("Value High", "Original", 255, 255, onChange)

# show default images
cv2.imshow("Original", img)
cv2.imshow("Result HSV", img_hsv)
cv2.imshow("Result RGB", img_rgb)

while True:
    # get trackbar values
    hue_val_low = cv2.getTrackbarPos("Hue Low", "Original")
    hue_val_high = cv2.getTrackbarPos("Hue High", "Original")
    saturation_val_low = cv2.getTrackbarPos("Saturation Low", "Original")
    saturation_val_high = cv2.getTrackbarPos("Saturation High", "Original")
    value_val_low = cv2.getTrackbarPos("Value Low", "Original")
    value_val_high = cv2.getTrackbarPos("Value High", "Original")

    # close if 'q' pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
