import numpy as np
import cv2 as cv
# cap = cv.VideoCapture('data/20/slow.flv')
cap = cv.VideoCapture(0)

def onchangepass(x):
    pass

def onchange(maxCorners,qualityLevel,minDistance,blockSize):
    #Check if parameter values are within range
    if blockSize<3:
        blockSize = 3
    if qualityLevel == 0:
        qualityLevel = 1
    if maxCorners == 0:
        maxCorners=1
    #Update Feature Parameters
    feature_params = dict( maxCorners = maxCorners,
                        qualityLevel = qualityLevel/10.0,
                         minDistance = minDistance,
                        blockSize = blockSize )
    # Parameters for lucas kanade optical flow
    lk_params = dict( winSize  = (15,15),
                  maxLevel = 2,
                  criteria = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))
    # Create random colors
    color = np.random.randint(0,255,(maxCorners,3))
    # Take first frame and find corners in it
    ret, old_frame = cap.read()
    old_gray = cv.cvtColor(old_frame, cv.COLOR_BGR2GRAY)
    p0 = cv.goodFeaturesToTrack(old_gray, mask = None, **feature_params)
    # Create a mask image for drawing purposes
    mask = np.zeros_like(old_frame)
    return lk_params,color,p0,mask,old_gray


# Create Named Window
cv.namedWindow("frame")
# Create Trackbars 
cv.createTrackbar("Max Corners", "frame", 200, 1000, onchangepass)
cv.createTrackbar("Quality Level", "frame", 3, 5, onchangepass)
cv.createTrackbar("Min Distance", "frame", 1, 51, onchangepass)
cv.createTrackbar("Block Size", "frame", 3, 51, onchangepass)

while(1):
    
    # Get trackbar positions
    maxCorners = cv.getTrackbarPos("Max Corners", "frame")
    qualityLevel = cv.getTrackbarPos("Quality Level", "frame")
    minDistance = cv.getTrackbarPos("Min Distance", "frame")
    blockSize = cv.getTrackbarPos("Block Size", "frame")

    # Call onchange function for calculating values
    lk_params,color,p0,mask,old_gray = onchange(maxCorners,qualityLevel,minDistance,blockSize)

    # Read frame
    ret,frame = cap.read()
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    
    # calculate optical flow
    p1, st, err = cv.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)

    # Select good points
    good_new = p1[st==1]
    good_old = p0[st==1]

    # draw the tracks
    for i,(new,old) in enumerate(zip(good_new,good_old)):
        a,b = new.ravel()
        c,d = old.ravel()
        mask = cv.line(mask, (a,b),(c,d), color[i].tolist(), 5)
        # frame = cv.circle(frame,(a,b),5,color[i].tolist(),-1)

    img = cv.add(frame,mask)
    cv.imshow('frame',img)

    k = cv.waitKey(30) & 0xff
    if k == 27:
        break
    # Now update the previous frame and previous points
    old_gray = frame_gray.copy()
    p0 = good_new.reshape(-1,1,2)

cv.destroyAllWindows()
cap.release()