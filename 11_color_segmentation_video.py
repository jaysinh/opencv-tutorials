import cv2
import numpy as np

# HSV value change listener
def onChange(x):
    pass

def getResultImages(image_hsv, hue_val_low, hue_val_high, saturation_val_low, saturation_val_high, value_val_low, value_val_high):
    low = (hue_val_low, saturation_val_low, value_val_low)
    high = (hue_val_high, saturation_val_high, value_val_high)
    
    mask = cv2.inRange(img_hsv, low, high)

    result_hsv = cv2.bitwise_and(img_hsv, img_hsv, mask=mask)
    result_rgb = cv2.cvtColor(result_hsv, cv2.COLOR_HSV2BGR)

    return result_hsv, result_rgb

# create a video capture object and load the camera
video_capture = cv2.VideoCapture(0)

# initialize display windows
cv2.namedWindow("Original")
cv2.namedWindow("Result RGB")
cv2.namedWindow("Result HSV")

# initialize trackbars
cv2.createTrackbar("Hue Low", "Original", 0, 255, onChange)
cv2.createTrackbar("Hue High", "Original", 255, 255, onChange)
cv2.createTrackbar("Saturation Low", "Original", 0, 255, onChange)
cv2.createTrackbar("Saturation High", "Original", 255, 255, onChange)
cv2.createTrackbar("Value Low", "Original", 0, 255, onChange)
cv2.createTrackbar("Value High", "Original", 255, 255, onChange)

# run an infinite loop
while True:
    status, img = video_capture.read()


    if status:
        img = cv2.blur(img, (5, 5))
        # create copies of image in RGB and HSV
        img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img_hsv = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2HSV)
        
        hue_val_low = cv2.getTrackbarPos("Hue Low", "Original")
        hue_val_high = cv2.getTrackbarPos("Hue High", "Original")
        saturation_val_low = cv2.getTrackbarPos("Saturation Low", "Original")
        saturation_val_high = cv2.getTrackbarPos("Saturation High", "Original")
        value_val_low = cv2.getTrackbarPos("Value Low", "Original")
        value_val_high = cv2.getTrackbarPos("Value High", "Original")

        result_hsv, result_rgb = getResultImages(img_hsv, hue_val_low, hue_val_high, saturation_val_low, saturation_val_high, value_val_low, value_val_high)

        # show default images
        cv2.imshow("Original", img)
        cv2.imshow("Result HSV", result_hsv)
        cv2.imshow("Result RGB", result_rgb)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# release the video capture object once we are done and destroy
# all windows
video_capture.release()
cv2.destroyAllWindows()
