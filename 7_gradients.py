"""
Load an image and calculate the pixel intensity gradients using Sobel. 
"""
# import libraries
import cv2
import numpy as np

# kernel size change listener for x direction
def onKChangeX(x):
    if k_size_x%2 != 0:
        sobel = cv2.Sobel(img, cv2.CV_8UC1, 1, 0, ksize=k_size_x)
        cv2.imshow("Sobel X", sobel)
        """
        As the result of sobel gives Black-to-White transition as positive slope
        and White-to-Black transition as negative slope, CV_8UC1 gives a zero value
        anything less than 0. Hence we only see the edges in one direction and not
        both. For this, we use a small hack where convert the values to a higher
        form such as CV_64F, find the absolutes, and then covert it back to CV_8UC1
        """
        # sobel = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=k_size_x)
        # abs_sobel64f = np.absolute(sobel)
        # sobel_8u = np.uint8(abs_sobel64f)
        # cv2.imshow("Sobel X", sobel_8u)

# kernel size change listener for y direction
def onKChangeY(x):
    if k_size_y%2 != 0:
        sobel = cv2.Sobel(img, cv2.CV_8UC1, 0, 1, ksize=k_size_y)
        cv2.imshow("Sobel Y", sobel)
        # abs_sobel64f = np.absolute(sobel)
        # sobel_8u = np.uint8(abs_sobel64f)
        # cv2.imshow("Sobel Y", sobel_8u)

# load image from cv2.imread(<path>)
img = cv2.imread("data/7/sudoku.jpeg", 0)

# create windows
cv2.namedWindow("Original")
cv2.namedWindow("Laplacian")
cv2.namedWindow("Sobel X")
cv2.namedWindow("Sobel Y")

# create trackbars
cv2.createTrackbar("K-Size X", "Original", 5, 21, onKChangeX)
cv2.createTrackbar("K-Size Y", "Original", 5, 21, onKChangeY)

# get the default values to show on window creation
laplacian = cv2.Laplacian(img, cv2.CV_8UC1)
sobel_x = cv2.Sobel(img, cv2.CV_8UC1, 1, 0, ksize=5)
sobel_y = cv2.Sobel(img, cv2.CV_8UC1, 0, 1, ksize=5)

# show image on window creation
cv2.imshow("Original", img)
cv2.imshow("Laplacian", laplacian)
cv2.imshow("Sobel X", sobel_x)
cv2.imshow("Sobel Y", sobel_y)

while True:
    # get trackbar values
    k_size_x = cv2.getTrackbarPos("K-Size X", "Original")
    k_size_y = cv2.getTrackbarPos("K-Size Y", "Original")

    # close if 'q' pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# close all windows
cv2.destroyAllWindows()