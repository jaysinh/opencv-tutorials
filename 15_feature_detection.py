import cv2
import numpy as np
#from matplotlib import pyplot as plt

def SIFT():
    # Read Image and convert to grayscale.
    img = cv2.imread('data/15/sift_image.jpg')
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    # Initiate detector
    sift = cv2.xfeatures2d.SIFT_create()
    
    # Detect Keypoints 
    kp = sift.detect(gray,None)

    # Draw keypoints
    cv2.drawKeypoints(gray,kp,img)

    # Write result into new image
    cv2.imwrite('data/15/keypoints_sift.jpg',img)


def SURF():
    img = cv2.imread('data/15/butterfly.jpg')
    
    # Initiate 
    surf = cv2.xfeatures2d.SURF_create()

    # Setting high threshold to reduce the number of keypoints in image for better visibility.
    surf.setHessianThreshold(50000)
    
    # Show all orientations in the same direction. 
    surf.setUpright(True)
    
    # Compute keypoints
    kp, des = surf.detectAndCompute(img,None)

    # Draw keypoints
    img2 = cv2.drawKeypoints(img,kp,None,(255,0,0),4)

    cv2.imwrite('data/15/keypoints_surf.jpg',img2)


def ORB():
    img = cv2.imread('data/15/simple.jpg')

    # Initiate detector
    orb = cv2.ORB_create()

    # Find the keypoints and compute the descriptors with ORB
    kp, des = orb.detectAndCompute(img, None)

    # draw keypoints
    img2 = cv2.drawKeypoints(img,kp,None)
    
    #plt.imshow(img2),plt.show()
    cv2.imwrite('data/15/keypoints_orb.jpg',img2)

# Main, call all functions
if __name__ == "__main__":
    SIFT()
    SURF()
    ORB()