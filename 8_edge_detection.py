"""
Load an image and perform edge detection using Canny-Edge detector
"""
# import libraries
import cv2
import numpy as np

def onThresholdChange(x):
    edges = cv2.Canny(img, thresh_1, thresh_2)
    cv2.imshow("Canny", edges)


# load image from cv2.imread(<path>)
img = cv2.imread("data/7/sudoku.jpeg", 0)

# create windows
cv2.namedWindow("Original")
cv2.namedWindow("Canny")

# create trackbars
cv2.createTrackbar("Thresh 1", "Canny", 1, 1000, onThresholdChange)
cv2.createTrackbar("Thresh 2", "Canny", 1, 1000, onThresholdChange)

edges = cv2.Canny(img, 100, 200)

# show image on window creation
cv2.imshow("Original", img)
cv2.imshow("Canny", edges)

while True:
    # get trackbar values
    thresh_1 = cv2.getTrackbarPos("Thresh 1", "Canny")
    thresh_2 = cv2.getTrackbarPos("Thresh 2", "Canny")

    # close if 'q' pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# close all windows
cv2.destroyAllWindows()