"""
Line detection in an image using Probabilistic Hough Transformation
"""
import cv2
import numpy as np

def onThresholdChange(x):
    img_res = img.copy()
    edges = cv2.Canny(gray, thresh_1, thresh_2)
    lines = cv2.HoughLinesP(edges, 1, np.pi/180, thresh_hough, minLineLength=min_line_len, maxLineGap=max_line_space)
    for line in lines:
        x1, y1, x2, y2 = line[0]
        cv2.line(img_res, (x1, y1), (x2, y2), (0, 255, 0), 2)
    cv2.imshow("Canny", edges)
    cv2.imshow("Hough Transform", img_res)

img = cv2.imread('data/10/road.jpg')
img = cv2.resize(img, (1000, 600))
img_result = img.copy()
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# create windows
cv2.namedWindow("Original")
cv2.namedWindow("Canny")
cv2.namedWindow("Hough Transform")

# create trackbars
cv2.createTrackbar("Thresh 1", "Canny", 352, 1000, onThresholdChange)
cv2.createTrackbar("Thresh 2", "Canny", 750, 1000, onThresholdChange)
cv2.createTrackbar("Thresh Hough", "Canny", 101, 1000, onThresholdChange)
cv2.createTrackbar("Min Line Length", "Canny", 194, 1000, onThresholdChange)
cv2.createTrackbar("Max Line Space", "Canny", 50, 1000, onThresholdChange)

# default data to display
img_res = img.copy()
edges = cv2.Canny(gray, 352, 750)
lines = cv2.HoughLinesP(edges, 1, np.pi/180, 101, minLineLength=194, maxLineGap=50)
for line in lines:
    x1, y1, x2, y2 = line[0]
    cv2.line(img_res, (x1, y1), (x2, y2), (0, 255, 0), 2)

cv2.imshow("Original", img)
cv2.imshow("Canny", edges)
cv2.imshow("Hough Transform", img_res)

while True:
    # get trackbar values
    thresh_1 = cv2.getTrackbarPos("Thresh 1", "Canny")
    thresh_2 = cv2.getTrackbarPos("Thresh 2", "Canny")
    thresh_hough = cv2.getTrackbarPos("Thresh Hough", "Canny")
    min_line_len = cv2.getTrackbarPos("Min Line Length", "Canny")
    max_line_space = cv2.getTrackbarPos("Max Line Space", "Canny")

    # close if 'q' pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# close all windows
cv2.destroyAllWindows()
