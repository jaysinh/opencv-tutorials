"""
Load an image from a given path and show it in a window.
"""
# library imports
import cv2

# load image from cv2.imread(<path>)
img = cv2.imread("data/1/lenna.png")

# show image by creating a new window called "image" and pass it the img variable
cv2.imshow("image", img)

# wait for any key press to close window
cv2.waitKey(0)
