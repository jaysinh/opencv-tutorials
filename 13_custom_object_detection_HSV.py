import cv2
import numpy as np

# HSV value change listener
def onChange(x):
    pass

def getResultImages(image_hsv, hue_val_low, hue_val_high, saturation_val_low, saturation_val_high, value_val_low, value_val_high):
    low = (hue_val_low, saturation_val_low, value_val_low)
    high = (hue_val_high, saturation_val_high, value_val_high)
    
    mask = cv2.inRange(img_hsv, low, high)

    result_hsv = cv2.bitwise_and(img_hsv, img_hsv, mask=mask)
    result_rgb = cv2.cvtColor(result_hsv, cv2.COLOR_HSV2BGR)

    return result_hsv, result_rgb

def getContours(image):
    w, h, d = image.shape
    if d != 1:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    ret, thresh = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)

    temp, cnts, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # print(len(cnts),len(hierarchy))

    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
    rects = []
    for c in cnts:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        rect = cv2.minAreaRect(c)
        rects.append(rect)
    return rects

# create a video capture object and load the camera
video_capture = cv2.VideoCapture(0)

# initialize display windows
cv2.namedWindow("Original")
cv2.namedWindow("Result RGB")
cv2.namedWindow("Result HSV")

# initialize trackbars
cv2.createTrackbar("Hue Low", "Original", 42, 255, onChange)
cv2.createTrackbar("Hue High", "Original", 101, 255, onChange)
cv2.createTrackbar("Saturation Low", "Original", 50, 255, onChange)
cv2.createTrackbar("Saturation High", "Original", 255, 255, onChange)
cv2.createTrackbar("Value Low", "Original", 176, 255, onChange)
cv2.createTrackbar("Value High", "Original", 255, 255, onChange)

# run an infinite loop
while True:
    status, img = video_capture.read()


    if status:
        img_blur = cv2.medianBlur(img, 21)
        # create copies of image in RGB and HSV
        img_rgb = cv2.cvtColor(img_blur, cv2.COLOR_BGR2RGB)
        img_hsv = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2HSV)
        
        hue_val_low = cv2.getTrackbarPos("Hue Low", "Original")
        hue_val_high = cv2.getTrackbarPos("Hue High", "Original")
        saturation_val_low = cv2.getTrackbarPos("Saturation Low", "Original")
        saturation_val_high = cv2.getTrackbarPos("Saturation High", "Original")
        value_val_low = cv2.getTrackbarPos("Value Low", "Original")
        value_val_high = cv2.getTrackbarPos("Value High", "Original")

        result_hsv, result_rgb = getResultImages(img_hsv, hue_val_low, hue_val_high, saturation_val_low, saturation_val_high, value_val_low, value_val_high)

        contours = getContours(result_rgb)

        for c in contours:
            box = cv2.boxPoints(c)
            box = np.int0(box)
            overlay = img.copy()
            cv2.drawContours(overlay, [box], 0, (0, 0, 255), -1)
            cv2.drawContours(overlay, [box], 0, (0, 0, 255), 2)
            img = cv2.addWeighted(img, 0.5, overlay, 0.5, 0)

        # show default images
        cv2.imshow("Original", img)
        cv2.imshow("Result HSV", result_hsv)
        cv2.imshow("Result RGB", result_rgb)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# release the video capture object once we are done and destroy
# all windows
video_capture.release()
cv2.destroyAllWindows()