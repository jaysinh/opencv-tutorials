import cv2
import sys
import time

 
if __name__ == '__main__' :
 
    # Set up tracker.
    # Instead of MIL, you can also use
    tracker_types = ['BOOSTING', 'MIL','KCF', 'TLD', 'MEDIANFLOW', 'GOTURN', 'MOSSE', 'CSRT']
    tracker_type = tracker_types[2]
    tracker = cv2.TrackerKCF_create()
    
    # Read video
    video = cv2.VideoCapture("data/17/leoedit.mp4")
 
    # Exit if video not opened.
    if not video.isOpened():
        print ("Could not open video")
        sys.exit()
    
    # Select and create the background subtraction object
    method = 1

    if method == 0:
        bgSubtractor = cv2.bgsegm.createBackgroundSubtractorMOG()
    elif method == 1:
        bgSubtractor = cv2.createBackgroundSubtractorMOG2()
    else:
        bgSubtractor = cv2.bgsegm.createBackgroundSubtractorGMG()
    
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    
    # Read first frame or use loop to skip to another frame.
    #for i in range(470):#470
    ok, frame = video.read()
    if not ok:
        print ('Cannot read video file')
        sys.exit()
    
    # Uncomment below lien to define an initial bounding box
    #bbox = (287, 23, 86, 320)
 
    # Select a bounding box
    bbox = cv2.selectROI(frame, False)
 
    # Initialize tracker with first frame and bounding box
    ok = tracker.init(frame, bbox)
 
    while True:
        # Read a new frame
        ok, frame = video.read()
        if not ok:
            break
        
        foregroundMask = bgSubtractor.apply(frame)
        
        # Remove part of the noise
        foregroundMask = cv2.morphologyEx(foregroundMask, cv2.MORPH_OPEN, kernel)
        

        # Start timer
        timer = cv2.getTickCount()

        time.sleep(0.02)
        # Update tracker
        ok, bbox = tracker.update(frame)
 
        # Calculate Frames per second (FPS)
        fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)
 
        # Draw bounding box
        if ok:
            # Tracking success
            p1 = (int(bbox[0]), int(bbox[1]))
            p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
            cv2.rectangle(foregroundMask, p1, p2, (255,0,0), 2, 1)
        else :
            # Tracking failure
            cv2.putText(foregroundMask, "Tracking failure detected", (100,80), cv2.FONT_HERSHEY_SIMPLEX, 0.75,(0,0,255),2)
 
        # Display tracker type on frame
        cv2.putText(foregroundMask, tracker_type + " Tracker", (100,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50),2)
     
        # Display FPS on frame
        cv2.putText(foregroundMask, "FPS : " + str(int(fps)), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)
 
       # Display result
        cv2.imshow('background subtraction', foregroundMask)
 
        # Exit if ESC pressed
        k = cv2.waitKey(1) & 0xff
        if k == 27 : break