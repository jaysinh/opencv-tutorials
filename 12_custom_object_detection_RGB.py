import cv2
import numpy as np

# HSV value change listener
def onChange(x):
    pass

def getResultImages(image_rgb, red_val_low, red_val_high, green_val_low, green_val_high, blue_val_low, blue_val_high):
    low = (red_val_low, green_val_low, blue_val_low)
    high = (red_val_high, green_val_high, blue_val_high)
    
    mask = cv2.inRange(image_rgb, low, high)

    result_rgb = cv2.bitwise_and(image_rgb, image_rgb, mask=mask)

    return result_rgb


def getContours(image):
    w, h, d = image.shape
    if d != 1:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    ret, thresh = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)

    temp, cnts, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # print(len(cnts),len(hierarchy))

    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
    rects = []
    for c in cnts:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        rect = cv2.minAreaRect(c)
        rects.append(rect)
    return rects

# create a video capture object and load the camera
video_capture = cv2.VideoCapture(0)

# initialize display windows
cv2.namedWindow("Original")
cv2.namedWindow("Result RGB")

# initialize trackbars
cv2.createTrackbar("Red Low", "Original", 200, 255, onChange)
cv2.createTrackbar("Red High", "Original", 255, 255, onChange)
cv2.createTrackbar("Green Low", "Original", 0, 255, onChange)
cv2.createTrackbar("Green High", "Original", 255, 255, onChange)
cv2.createTrackbar("Blue Low", "Original", 255, 255, onChange)
cv2.createTrackbar("Blue High", "Original", 255, 255, onChange)

# run an infinite loop
while True:
    status, img = video_capture.read()


    if status:
        red_val_low = cv2.getTrackbarPos("Red Low", "Original")
        red_val_high = cv2.getTrackbarPos("Red High", "Original")
        green_val_low = cv2.getTrackbarPos("Green Low", "Original")
        green_val_high = cv2.getTrackbarPos("Green High", "Original")
        blue_val_low = cv2.getTrackbarPos("Blue Low", "Original")
        blue_val_high = cv2.getTrackbarPos("Blue High", "Original")
    
        img_blur = cv2.medianBlur(img, 21)
        result_rgb = getResultImages(img_blur, red_val_low, red_val_high, green_val_low, green_val_high, blue_val_low, blue_val_high)

        contours = getContours(result_rgb)

        for c in contours:
            box = cv2.boxPoints(c)
            box = np.int0(box)
            overlay = img.copy()
            cv2.drawContours(overlay, [box], 0, (0, 0, 255), -1)
            cv2.drawContours(overlay, [box], 0, (0, 0, 255), 2)
            img = cv2.addWeighted(img, 0.5, overlay, 0.5, 0)

        # show default images
        cv2.imshow("Original", img)
        cv2.imshow("Result RGB", result_rgb)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# release the video capture object once we are done and destroy
# all windows
video_capture.release()
cv2.destroyAllWindows()
