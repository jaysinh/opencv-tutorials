"""
Load a video file from directory and show it frame by frame in a window
"""
# import libraries
import cv2

# create a video capture object and load the video
video_capture = cv2.VideoCapture("data/2/forest.mov")

# run an infinite loop
while True:
    # capture a frame from the video. This returns a
    # boolean status True/False and an image matrix object
    # as a frame
    status, frame = video_capture.read()

    # check if fram load status is true. Can also be written
    # as if status == True
    if status:
        # create a window called Video and show the frame
        cv2.imshow("Video", frame)
        # wait for 1ms per frame to see of there is any input
        # here we have set 'q' as an input to quit the video
        # stream
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# release the video capture object once we are done and destroy
# all windows
video_capture.release()
cv2.destroyAllWindows()
