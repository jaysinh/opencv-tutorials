"""
Load an image and apply a combination of blurs
"""
# library imports
import cv2

# Handler for value change for Gaussian Blur trackbar
def onGaussianChange(x):
    # as kernal size has to be odd numbers, we skip even number values
    if g_blur_val%2 != 0:
        # apply and show blur
        blur = cv2.GaussianBlur(img, (g_blur_val, g_blur_val), 0)
        cv2.imshow("Gaussian Blur", blur)

# Handler for value change for Median Blur trackbar
def onMedianChange(x):
    # as kernal size has to be odd numbers, we skip even number values
    if m_blur_val%2 != 0:
        # apply and show blur
        blur = cv2.medianBlur(img, m_blur_val)
        cv2.imshow("Median Blur", blur)

# Handler for value change for Bilateral Blur trackbar
def onBilateralChange(x):
    # apply and show blur
    blur = cv2.bilateralFilter(img, 9, b_blur_val, b_blur_val)
    cv2.imshow("BiLateral Blur", blur)

# Handler for value change for Average Blur trackbar
def onAveragingChange(x):
    # as kernal size has to be odd numbers, we skip even number values
    if a_blur_val%2 != 0:
        # apply and show blur
        blur = cv2.blur(img, (a_blur_val, a_blur_val))
        cv2.imshow("Average Blur", blur)

# load image from cv2.imread(<path>)
img = cv2.imread("data/1/lenna.png")

# create windows
cv2.namedWindow("Gaussian Blur")
cv2.namedWindow("Median Blur")
cv2.namedWindow("BiLateral Blur")
cv2.namedWindow("Average Blur")

# create trackbars
cv2.createTrackbar("gaussian", "Gaussian Blur", 1, 51, onGaussianChange)
cv2.createTrackbar("median", "Median Blur", 1, 51, onMedianChange)
cv2.createTrackbar("bilateral", "BiLateral Blur", 75, 299, onBilateralChange)
cv2.createTrackbar("averaging", "Average Blur", 1, 51, onAveragingChange)

# show image on window creation
cv2.imshow("Gaussian Blur", img)
cv2.imshow("Median Blur", img)
cv2.imshow("BiLateral Blur", img)
cv2.imshow("Average Blur", img)

while True:
    # get trackbar values
    g_blur_val = cv2.getTrackbarPos("gaussian", "Gaussian Blur")
    m_blur_val = cv2.getTrackbarPos("median", "Median Blur")
    b_blur_val = cv2.getTrackbarPos("bilateral", "BiLateral Blur")
    a_blur_val = cv2.getTrackbarPos("averaging", "Average Blur")

    # close if 'q' pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# close all windows
cv2.destroyAllWindows()